<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Condition main class.
 *
 * @package availability_groupsubstring
 * @copyright 2023 Nicholas van Rheede van Oudtshoorn < nicholas@pbc.wa.edu.au >

 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace availability_groupsubstring;

/**
 * Condition main class.
 *
 * @package availability_groupsubstring
 * @copyright 2023 Nicholas van Rheede van Oudtshoorn < nicholas@pbc.wa.edu.au >

 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class condition extends \core_availability\condition {
    /** @var array Array from group id => name */
    protected static $groupnames = array();

    /** @var string Substring required of this group, or '' for any group */
    protected $groupsubstring;

    /**
     * Constructor.
     *
     * @param \stdClass $structure Data structure from JSON decode
     * @throws \coding_exception If invalid data structure.
     */
    public function __construct($structure) {
        // Get group id (the substring being searched for).
        if (!property_exists($structure, 'id')) {
            $this->groupsubstring = '';
        } else {
            $this->groupsubstring = $structure->id;
        }
    }

    public function save() {
        $result = (object)array('type' => 'groupsubstring');
        if ($this->groupsubstring) {
            $result->id = $this->groupsubstring;
        }
        return $result;
    }

    public function is_available($not, \core_availability\info $info, $grabthelot, $userid) {
        $course = $info->get_course();
        $context = \context_course::instance($course->id);
        $allow = true;
        if (!has_capability('moodle/site:accessallgroups', $context, $userid)) {
            // Get all groups the user belongs to.
            if (count(self::$groupnames) == 0) {
                $course = $info->get_course();
                $this->get_group_names($course->id);
            }

            $groups = $info->get_modinfo()->get_groups();
            $mygroups = array();
            foreach ($groups as $gid) {
                if (array_key_exists($gid, self::$groupnames)) {
                    $mygroups[$gid] = self::$groupnames[$gid];
                }
            }

            if ($this->groupsubstring != '') {
                $allow = false;

                $uppergroupsubstring = strtoupper($this->groupsubstring);
                foreach ($mygroups as $id => $groupname) {
                    if (!empty($groupname) && (strpos(strtoupper($groupname), $uppergroupsubstring) !== false)) {
                        $allow = true;
                        break;
                    }

                }
            }

            // The NOT condition applies before accessallgroups (i.e. if you
            // set something to be available to those NOT in group X,
            // people with accessallgroups can still access it even if
            // they are in group X).
            if ($not) {
                $allow = !$allow;
            }
        }
        return $allow;
    }

    public function get_description($full, $not, \core_availability\info $info) {
        global $DB;

        if ($this->groupsubstring == '') {
            return get_string(
                $not ? 'requires_notanygroup' : 'requires_anygroup',
                'availability_groupsubstring'
            );
        }

        return get_string(
            $not ? 'requires_notgroup' : 'requires_group',
            'availability_groupsubstring',
            $this->groupsubstring
        );
    }

    private function get_group_names($courseid) {
        global $DB;

        $coursegroups = $DB->get_records(
            'groups',
            array('courseid' => $courseid),
            '',
            'id, name'
        );
        foreach ($coursegroups as $rec) {
            self::$groupnames[$rec->id] = $rec->name;
        }
    }

    protected function get_debug_string() {
        return $this->groupsubstring ? '#' . $this->groupsubstring : 'any';
    }

    public function include_after_restore($restoreid, $courseid, \base_logger $logger,
            $name, \base_task $task) {
        return !$this->groupsubstring || $task->get_setting_value('groups');
    }

    public function update_after_restore($restoreid, $courseid, \base_logger $logger, $name) {
        // global $DB;
        // if (!$this->groupsubstring) {
        // return false;
        // }
        // $rec = \restore_dbops::get_backup_ids_record($restoreid, 'groupsubstring', $this->groupsubstring);
        // if (!$rec || !$rec->newitemid) {
        // If we are on the same course (e.g. duplicate) then we can just use the existing one.
        // if ($DB->record_exists(
        // 'groupsubstring',
        // array('id' => $this->groupsubstring, 'courseid' => $courseid)
        // )) {
        // return false;
        // }
        // Otherwise it's a warning.
        // $this->groupsubstring = '';
        // $logger->process(
        // 'Restored item (' . $name .
        // ') has availability condition on group that was not restored',
        // \backup::LOG_WARNING
        // );
        // } else {
        // $this->groupsubstring = $rec->newitemid;
        // }
        // return true;
        return false;
    }

    public function update_dependency_id($table, $oldid, $newid) {
        if ($table === 'groupsubstring' && $this->groupsubstring === $oldid) {
            $this->groupsubstring = $newid;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Wipes the static cache used to store grouping names.
     */
    public static function wipe_static_cache() {
        self::$groupnames = array();
    }

    public function is_applied_to_user_lists() {
        // Group conditions are assumed to be 'permanent', so they affect the
        // display of user lists for activities.
        return true;
    }

    public function filter_user_list(
        array $users,
        $not,
        \core_availability\info $info,
        \core_availability\capability_checker $checker
    ) {
        global $CFG, $DB;

        // If the array is empty already, just return it.
        if (!$users) {
            return $users;
        }

        require_once($CFG->libdir . '/grouplib.php');
        $course = $info->get_course();
        // List users for this course who match the condition.
        $groupusers = array();
        if ($this->groupsubstring) {
            $uppergroupsubstring = strtoupper($this->groupsubstring);

            if (count(self::$groupnames) == 0) {
                $course = $info->get_course();
                $this->get_group_names($course->id);
            }

            foreach (self::$groupnames as $gid => $groupname) {
                if (strpos(strtoupper($groupname), $uppergroupsubstring) !== false) {
                    $groupinfo = groups_get_members($gid, 'u.id', 'u.id ASC');
                    foreach ($groupinfo as $gid => $uinfo) {
                        $groupusers[$gid] = $gid;
                    }
                }
            }
        }

        // List users who have access all groups.
        $aagusers = $checker->get_users_by_capability('moodle/site:accessallgroups');
        // Filter the user list.
        $result = array();
        foreach ($users as $id => $user) {
            // Always include users with access all groups.
            if (array_key_exists($id, $aagusers)) {
                $result[$id] = $user;
            } else if (in_array($id, $groupusers)) {
                // Other users are included or not based on group membership.
                $result[$id] = $user;
            }
        }
        return $result;
    }

    /**
     * Returns a JSON object which corresponds to a condition of this type.
     *
     * Intended for unit testing, as normally the JSON values are constructed
     * by JavaScript code.
     *
     * @param int $groupid Required group id (0 = any group)
     * @return stdClass Object representing condition
     */
    public static function get_json($groupsubstring = '') {
        $result = (object)array('type' => 'groupsubstring');
        // Id is only included if set.
        if ($groupsubstring != '') {
            $result->id = $groupsubstring;
        }
        return $result;
    }

    public function get_user_list_sql($not, \core_availability\info $info, $onlyactive) {
        global $DB;

        // Get enrolled users with access all groups. These always are allowed.
        list($aagsql, $aagparams) = get_enrolled_sql(
            $info->get_context(),
            'moodle/site:accessallgroups',
            0,
            $onlyactive
        );

        // Get all enrolled users.
        list($enrolsql, $enrolparams) =
            get_enrolled_sql($info->get_context(), '', 0, $onlyactive);

        // Condition for specified or any group.
        $matchparams = array();
        if ($this->groupsubstring) {
            $uppergroupsubstring = strtoupper($this->groupsubstring);
            $matchsql = "SELECT 1
                           FROM {groups_members} gm
                          WHERE gm.userid = userids.id";
            if (count(self::$groupnames) == 0) {
                $course = $info->get_course();
                $this->get_group_names($course->id);
            }
            foreach (self::$groupnames as $gid => $groupname) {
                if (strpos(strtoupper($groupname), $uppergroupsubstring)) {
                    $matchsql .= " AND gm.groupid = " . self::unique_sql_parameter($matchparams, $gid);
                }
            }
        } else {
            $matchsql = "SELECT 1
                           FROM {groups_members} gm
                           JOIN {groups} g ON g.id = gm.groupid
                          WHERE gm.userid = userids.id
                                AND g.courseid = " .
                self::unique_sql_parameter($matchparams, $info->get_course()->id);
        }

        // Overall query combines all this.
        $condition = $not ? 'NOT' : '';
        $sql = "SELECT userids.id
                  FROM ($enrolsql) userids
                 WHERE (userids.id IN ($aagsql)) OR $condition EXISTS ($matchsql)";
        return array($sql, array_merge($enrolparams, $aagparams, $matchparams));
    }
}
